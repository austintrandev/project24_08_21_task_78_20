--Test 1
SELECT * FROM `students`;
--Test 2
SELECT st.student_code,st.user_name FROM students st;
--Test 3
SELECT * FROM subjects sj WHERE sj.credit > 3;
--Test 4
SELECT * FROM grades g WHERE g.exam_date < '2021/05/01';
--Test 5
SELECT st.student_code,g.subject_id,g.grade,g.exam_date FROM grades g 
INNER JOIN students st
ON g.student_id = st.id;
--Test 6
SELECT st.student_code, 
CONCAT(st.first_name,' ',st.last_name) as fullname,g.subject_id,g.grade,
g.exam_date FROM grades g 
INNER JOIN students st
ON g.student_id = st.id
WHERE g.subject_id = 1;
--Test 7
SELECT sj.subject_name, st.student_code, 
CONCAT(st.first_name,' ',st.last_name) as ho_va_ten, g.grade, g.exam_date
FROM (grades g JOIN students st ON g.student_id = st.id
JOIN subjects sj ON g.subject_id = sj.id);
--Test 8
SELECT g.subject_id,COUNT(g.student_id) AS student_number
FROM grades g
GROUP BY g.subject_id;
--Test 9
SELECT g.subject_id,COUNT(g.student_id) AS student_number
FROM grades g
GROUP BY g.subject_id
HAVING student_number > 5;
--Test 10
SELECT sj.subject_name,
COUNT(g.student_id) AS student_number
FROM grades g JOIN subjects sj 
ON g.subject_id = sj.id 
GROUP BY g.subject_id
HAVING student_number > 5;
--Test 11
SELECT sj.subject_name,CONCAT(st.first_name, ' ',st.last_name) as fullname, g.grade,g.exam_date 
FROM grades g 
JOIN students st ON g.student_id = st.id
JOIN subjects sj ON g.subject_id = sj.id
ORDER BY g.grade DESC
LIMIT 3;

